![Header](img/header.jpg)

# GoldStarTapeEncoder

This is the subtitles encoder for [Korean GoldStar AHA-C380 cassette walkman](https://www.youtube.com/watch?v=UUxAdcdCazo).
There's another model of GoldStar walkman which supports that, but I forgot the
model number.

It comes bundled with an English textbook and quite a lot of listening material,
which is recorded onto 8 cassette tapes with have both voice and data tracks.

In order to display the "captions" read from the data track, this walkman is
equipped with a small LCD screen.

Some time back in 2016 when this thing caught my eye again, and I decided that I
want to be able to display my own things on it for my amusement :^) .

# Technical stuff/theory of operation

This walkman has 3 modes:

* FM/AM radio;
* normal tape player;
* caption tape player.

In radio or normal player mode, all data decoding circuitry and LCD screen are
powered off. Any stereo audio is being fed normally into stereo headphones.

In the caption mode, stereo track being played contains normal analog audio for
the user to listen to on the right channel, and data for the player to decode
and display on the left channel.

In order to decode and display the data, this walkman is equipped with
8051-compatible microcontroller and HD44780-standard 2x20 display. As far as I
know, this microcontroller has either OTP EEPROM or mask ROM, and I've found no
way to download or change its firmware, atleast in-circuit.

The modulation used for the data is simple binary FSK, with '1' bit represented
by 2 cycles of 2400Hz and '0' bit represented by 1 cycle of 1200Hz. There's no
further error-correction encoding or scrambling applied to the data.

## Block structure

* The data is split into blocks, separated by silence/no signal;
* Each block represents a single display of text.
* Each block may be fully represented by a stream of bits modulated with
aforementioned FSK modulation;
* The block of data begins with a *pilot tone*, which consists of a stream of
'1' bits. The minimum amount of pilot tone bits is not determined yet, but 90ms
worth of 'ones' seems to be sufficient. A longer pilot tone may be used;
* After the pilot tone, *data bytes* begin. The amount of data bytes doesn't
seem to be limited by anything but the capacity of RAM of the microcontroller
itself. Big enough amounts of data can hang the CPU, which would then require
a power cycle (that is, 'stop' and 'play'). Usually, there are 22 bytes of data
for each screen;
* Each data byte is prefixed by bits '10' "byte start" mark and is suffixed by
bit '1' "byte end" mark;
* Each data byte is stored LSB first;
* First data byte is 0x20, which seems to not affect anything at all and is
constant on all the original English lesson recordings;
* Second data byte specifies the transition (or rather, appearance :^) ) effect.
So far, I've been able to find the following useful combinations:

| Byte | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 | Description |
|------|---|---|---|---|---|---|---|---|---------------------------------------|
| 0xE0 | 1 | 1 | 1 | 0 | 0 | 0 | 0 | 0 | No effect, text appears immediately |
| 0xE8 | 1 | 1 | 1 | 0 | 1 | 0 | 0 | 0 | Bottom line rolls in from the right side of the screen |
| 0xF0 | 1 | 1 | 1 | 1 | 0 | 0 | 0 | 0 | Slow letter-by-letter reveal in place, from left to right, from top to bottom |
| 0xF8 | 1 | 1 | 1 | 1 | 1 | 0 | 0 | 0 | Letters falling into place from the right side of the screen |
|      | x | x | x | x | x | x | x | 1 | LCD turns off (contrast voltage disappears) for a split second. LCD reset, perhaps? |

* Last 2 bytes of data are constant zeroes, which are most likely serve as block
terminators;
* All the bytes inbetween all of the above are bytes which go to the screen. No
encoding conversion is applied to those bytes. Their representation is defined
by LCD character generator (basically, see any Japanese HD44780-compatibler
controller datasheet), which in our case means ASCII + some Japanese alphabet;
* **Delay between data blocks should account for the time it takes for selected
effect to complete before next data block starts.** Firmware of the
microcontroller can animate a chosen effect or read from tape, but not both at
the same time. Blocks packed toghether without any delay at all don't make sense
anyway, unless you're trying to animate a video on the display :^)

# What's been done so far

**DISCLAIMER: THIS SOFTWARE ISN'T DONE AND READY FOR ANYTHING AT THIS POINT -
THOSE ARE ONLY MY SHODDILY-WRITTEN AGE-OLD EXPERIMENTS. I DON'T GUARANTEE
ANYTHING. USE AT YOUR OWN RISK.**

## Prerequisites
* Some FFmpeg version installed; Scripts pipe into it to be able to use
different audio sources and formats, also to write generated data blocks; Having
it accessible through the PATH enviroment variable is nice too :^) ;
* Python 3.x.

## main.py

Python script that extracts data from available recordings into a LyRiCs file. 

Syntax:

`./main.py <input_audio_file> <channel_number> <output_lrc_file>`

There's no error hadnling yet, but it works good enough with correctly
specified channel. May require extra manual amplification of data track for now,
e.g. using 'normalize' in Audacity.

You can see the only video showing working decoding in action
[here](https://www.youtube.com/watch?v=ko0lzg0Nyy0).

### example_output.lrc
This is what the resulting output file from the script above looks like.

## tape\_freq\_gen.py
This is the block generator script that emits a sound file with specified data
track. At the moment, this script cannot be used straight as an utility, it only
contains experiments inside.

### Usage
A stream of blocks can be generated by an instance of GoldStarTapeEncoder class.

The most important methods are:

* `add_silence(self, ms)`

* `encode_message(self, header_byte=0x20, effect_byte=0xe0, message="")`

* `write(self, filename)`

The generated data blocks are stored as a bytearray of audio samples. Each time
`add_silence()` or `encode_message()` are called, this bytearray is appended 
with more samples. Once `write()` is called, the entire bytearray is passed to
FFmpeg to store in some usable audio format (which is, at the moment, hardcoded
to 8-bit unsigned, mono, 16kHz WAV).

# Generated examples

If you have such a walkman, you can record the files below onto the cassette 
tape (or put them onto a cassette MP3/bluetooth player/adapter thingy you can
buy from Aliexpress or elsewhere) to try them out:

* [Character generator test](https://drive.google.com/open?id=1JjNSn0v1gIjuEjWzwXNG_K5ZyR-9vCiy)
— displays the 256 possible characters 16 at a time;

* [Effects test](https://drive.google.com/open?id=1M9vDjn3UqzdhwIn6KK4dwJu-WWkVOUZ2)
— attempt to iterate through all possible effect variations;

* [1st byte test](https://drive.google.com/open?id=1zOD7LKKcAXopFYCSe95cIoYn2BV1vmBe)
— iterating through all values of 1st byte in an attempt to see if it changes
anything or writes to CGRAM.

# Other stuff

* [Original English course tape set](https://drive.google.com/drive/folders/103-9SYczixVL83dzoakxT2hnPJtc5mTL)
