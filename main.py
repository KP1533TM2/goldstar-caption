#!/bin/python
import subprocess
import os
import sys
from math import modf
import re
import string 
# TODO: Sanitize CLI input, add argument to select channel and stuff
filename = sys.argv[1]
channel = sys.argv[2]
lrc_filename = sys.argv[3]
command = ["ffmpeg -i \"{0}\" -ar 16000 -map_channel 0.0.{1} -f u8 pipe:1".format(filename,channel)]
#command = ["ffmpeg -f alsa -i \"{0}\" -ar 16000 -map_channel 0.0.{1} -f u8 pipe:1".format(filename,channel)]

print(command)

p = subprocess.Popen(command,stdout=subprocess.PIPE, shell=True, universal_newlines=False)




class GoldStarTapeReader:
	'GoldStar tape reader'
	lowFreq = 1200
	highFreq = 2400
	deviation = 60
	amplitude = 6
	wipes_dictionary = {
		0xa8:"Bottom line scrolls from the right (same as 0xe8)",
		0xe0:"Normal immediate appearance",
		0xe8:"Bottom line scrolls from the right",
		0xf0:"Slow letter-by-letter reveal in place, from left to right, from top to bottom",
		0xf2:"Slow letter-by-letter reveal in place, from left to right, from top to bottom (same as 0xf0)",
		0xf8:"Letters falling into place from the right side of the screen"
	}
	def __init__(self, sampleRate, callback, debug):
		self.sampleRate = sampleRate
		self.slowPeriod = sampleRate / GoldStarTapeReader.lowFreq
		self.fastPeriod = sampleRate / GoldStarTapeReader.highFreq
		self.sampleTime = 1.0/sampleRate
		self.timer = 0.0
		self.callback = callback
		self.debug = debug
		self.debugPrint("Init, sample rate {0} Hz, low period {1}, high period {2}".format(self.sampleRate, self.slowPeriod, self.fastPeriod))
		self.lowThreshold = int(255/2-GoldStarTapeReader.amplitude/2)
		self.highThreshold  = int(255/2-GoldStarTapeReader.amplitude/2)
		self.state = 0
		self.sampleCount = 0
		self.previousSample = False
		self.currentSample = False
		self.msg_bits = []
		self.startbytes = []
		
	def sample(self,u8):
		"""The 'analogue' decoding part, if you will. Makes a list of wavelengths"""
		# Some hysteresis
		if(u8 < self.lowThreshold):
			self.currentSample = False
			if(self.currentSample != self.previousSample):
				self.addWaveByLength(self.sampleCount)
#				self.debugPrint("{0} => \t {1}".format(self.timer, self.sampleCount))
			self.sampleCount = 0
		if(u8 > self.highThreshold):
			self.currentSample = True
		self.sampleCount += 1		
		self.previousSample = self.currentSample
		# State machine, kinda
#		self.debugPrint("{0} => \t {1}".format(self.timer, self.currentSample))
		self.timer += self.sampleTime
		
	def addWaveByLength(self,samples):
		norm = float(samples) - self.fastPeriod
		##print(norm)
		if(norm <= 0):
			self.msg_bits.append(True)
		elif norm > 0 and norm <= self.fastPeriod:
			self.msg_bits.append(False)
		else:
			x = self.decode_bits_to_bytes(self.decode_bitlengths(self.msg_bits))
			if x != b'':
				self.startbytes.append([x[0], x[1]])
				x.pop()
				x.pop()	# remove couple of NULs at the end
				self.debugPrint("at {0}:".format(self.timer))
				self.debugPrint("start bytes => \t {1}, {2}".format(self.timer, hex(x[0]), hex(x[1])))
				if x[1] in GoldStarTapeReader.wipes_dictionary:
					effect = GoldStarTapeReader.wipes_dictionary[x[1]]
				else:
					effect = "unknown"
				self.debugPrint("effect => \t {0}".format(effect))
				self.debugPrint("message line => \t {0}".format(x[2:22].decode("utf-8")))
				self.debugPrint("message line => \t {0}".format(x[22:].decode("utf-8")))
				lines = x[2:].decode("utf-8").replace('\x20','\xa0')
				self.callback(self.timer,lines)
				
			self.msg_bits = []
		
	def decode_bitlengths(self,wavelengths):
		state = 0
		pilotTonePulses = 10 # off the top of my head
		pulses = 0
		bits = []
		for wv in wavelengths:
			if state == 0:	# search for pilot tone
				if wv == True:
					if (pulses < pilotTonePulses):
						pulses += 1
					else:
						# found it!
						pulses = 0
						state = 10	
				else:
					pulses = 0
			elif state == 1:	# decode bits
				if not wv:
					bits.append(False)
				else:
					state = 2
			elif state == 2:
				if wv:
					bits.append(True)
					state=1
				else:
					# something's out of sync
					bits.append(None)
					state=1
					#return []
			elif state == 10:
				# waiting for pilot to be over
				if not wv:
					# needed for synchro pulses
					bits.append(True)
					bits.append(False)
					state = 1
		return bits
	def decode_bits_to_bytes(self,bits):
		state = 0
		byte_val = 0
		bytes = bytearray()
		bit_count = 0
		for bit in bits:
			if state==0 and bit==True: state = 1
			elif state==1 and bit==False:
				state = 2
				bit_count = 8
			elif state==1 and bit==True: state = 0
			elif state==2:
				byte_val = (byte_val>>1)|(0x80 if bit else 0)
#				print(bit)
				if(bit_count == 1):
					state=3
					bytes.append(byte_val)
					#print(hex(byte_val))
				else:
					bit_count-=1
			elif state==3 and bit==True: state=0
		#print bytes
		return bytes
	
	def debugPrint(self,s):
		if(self.debug):
			print("[{0}] {1}".format(self.__class__.__name__,s))
		

def cb(milliseconds, message):
	print(milliseconds, message)

def lrcPrint(time_seconds,lines):
	# Replace every space by with no-break space so
	# subtitle viewer won't mess with the lines, hopefully
	# works with mpv, atleast
	line1 = re.sub('\s\s+', '\xa0', lines[0:20].strip())
	line2 = re.sub('\s\s+', '\xa0', lines[20:40].strip())
#	line1 = lines[0:20].ljust(20).replace('\x20','\xa0')
#	line2 = lines[20:40].ljust(20).replace('\x20','\xa0')
	reformat = line1+' '+line2
	m = time_seconds // 60
	ms,s = modf(time_seconds - m*60) 
	f.write('[{:02d}:{:02d}.{:02d}]{}\n'.format(int(m), int(s), int(ms*100),reformat))

reader = GoldStarTapeReader(16000, lrcPrint, False)

minv = 255
maxv = 0

f = open(lrc_filename, "w")


while True:
	out = p.stdout.read(1)
	if out != b'':
		if(out[0]>maxv):
			maxv = out[0]

		if(out[0]<minv):
			minv = out[0]

		reader.sample(out[0])
	else:
		if p.poll() is not None:
			#print("start bytes")
			#print(reader.startbytes)
			break



f.close()

#print(maxv, minv)
