from tape_freq_gen import TapeFreqGen
 

class FSKGen:
	'FSK modulation generator'
	def __init__(self, sampleRate=16000, scheme):
		"""Class constructor. scheme - dictionary with keys True ('1') and False ('0')"""
		self.scheme = scheme
		self.freq_gen = TapeFreqGen(sampleRate=sampleRate)
		self.waveform = []

	def reset():
		""" clear waveform storage """
		self.waveform = []
		
	def put_bit(bit_value):
		"""Output bit value"""
		self.freq_gen.add_wave(freq=self.scheme[bit_value][freq],periods=self.scheme[bit_value][periods])
		
	


x = TapeFreqGen(sampleRate=44100, waveform=TapeFreqGen.waveform_sine)

two_sines = x.add_wave(freq = 1200, periods = 1)
two_sines += x.add_wave(freq = 2400, periods = 2)
print(len(two_sines))
for s in two_sines:
	print(" "*(s>>1),"*")

