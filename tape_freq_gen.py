from math import sin, pi
import subprocess
import os
import sys

class WaveformGen:
	SINE = 0
	SILENCE = 5
	def __init__(self, sample_rate=16000, amplitude=50, phase=0, waveform=0):
		"""Class constructor: set default settings"""
		self.sample_rate = sample_rate
		self.amplitude = amplitude
		self.phase = phase
		self.waveform = waveform
		self.__job = []
	
	def reset_job():
		"""Clear job"""
		self.__job = []		
	
	def add_wave(self, frequency, periods, amplitude=None, phase=None, waveform=None):
		"""Add wave to the job"""
		self.__job.append({
			"waveform":(self.waveform if waveform is None else waveform),
			"amplitude":(self.amplitude if amplitude is None else amplitude),
			"phase":(self.phase if phase is None else phase),
			"frequency":frequency, "periods":periods})

	def add_silence(self, ms):
		"""Add silence"""
		self.add_wave(frequency=1000, periods=ms, waveform=WaveformGen.SILENCE)


	def gen_wave(self, desc):
		total_samples = int(self.sample_rate*(1/desc["frequency"])*desc["periods"])
		if desc["waveform"] == WaveformGen.SINE:
			amplitude = desc["amplitude"]/2
			phase = desc["phase"]/180*pi
			return bytearray([int(sin(phase+(x/total_samples)*2*pi*desc["periods"])*amplitude+127) for x in range(total_samples)])
		elif desc["waveform"] == WaveformGen.SILENCE:
			return bytearray([127]*total_samples)
		else:
			print("WaveformGen: unknown waveform type {0}; ignoring").format(desc["waveform"])
			return bytearray()
			

	def write(self, filename):
		"""Write out job to the file using ffmpeg"""
		command = ["ffmpeg -y -f u8 -ar {freq} -ac 1 -i pipe:0 {fname}".format(fname=filename, freq=self.sample_rate)]
		p = subprocess.Popen(command,stdin=subprocess.PIPE, shell=True,bufsize=0)
		waves = 0
		for wave in self.__job:
			waves+=1
			p.stdin.write(self.gen_wave(wave))
		p.stdin.close()
		p.wait()

class BinaryFSKGen(WaveformGen):
	def __init__(self,
			sample_rate=2400*8,
			amplitude=50,
			phase=0,
			waveform=0,
			schema={True:{"frequency":2400,"periods":8},False:{"frequency":1200,"periods":4}}):
		WaveformGen.__init__(self,
			sample_rate=sample_rate,
			amplitude=amplitude,
			phase=phase,
			waveform=waveform)
		self.schema = schema
		
	def add_binary_fsk(self, bitval):
		"""Add binary FSK encoded bit according to schema"""
		bit_schema = self.schema[bitval]
		self.add_wave(frequency=bit_schema["frequency"], periods=bit_schema["periods"])


class GoldStarTapeEncoder(BinaryFSKGen):
	def __init__(self):
		BinaryFSKGen.__init__(self,
			sample_rate=2400*8,
			amplitude=50,
			phase=180,
			waveform=WaveformGen.SINE,
			schema={True:{"frequency":2400,"periods":2},False:{"frequency":1200,"periods":1}})

	def create_pilot_tone(self, periods=100):
		"""Pilot tone - just a big bunch of '1's """
		for x in range(periods):
			self.add_binary_fsk(True)
			
	def encode_byte(self, byte_value):
		"""Encode byte with all the needed start and stop sync bits"""
		value = byte_value
		bits = 8
		# Byte start sequence
		self.add_binary_fsk(True)
		self.add_binary_fsk(False)
		# Byte itself
		while bits > 0:
			self.add_binary_fsk(True if value&1 else False)
			value>>=1
			bits-=1
		# Byte stop sequence
		self.add_binary_fsk(True)
			
	def encode_message(self, header_byte=0x20, effect_byte=0xe0, message=""):
		self.create_pilot_tone()
		self.encode_byte(header_byte)
		self.encode_byte(effect_byte)
		for c in message:
			self.encode_byte(ord(c))
		self.encode_byte(0)
		self.encode_byte(0)
		

w = GoldStarTapeEncoder()

def test1():
	"""Test that allowed me to see garbage in LCD's CGRAM, meaning it's CPU doesn't sanitize
	or do anything 'smart' with text data"""
	for c1 in range(0,256,16):
		string1 = ""
		string2 = "{0}...{1}".format(hex(c1),hex(c1+15))
		for c2 in range(16):
			string1 += chr(c1+c2)
		total = string1+" "*(20-len(string1))+string2
		w.encode_message(message=total)
		w.add_silence(2000)

def test2():
	"""Test that helped me figure out bits 3 and 4 as effect bits,
	and also bit 0 does something idk, maybe LCD reset or some crap"""
	for cmd_byte in range(256):
		string1 = ""
		string2 = "Cmd byte test {0}".format(hex(cmd_byte))
		for c1 in range(16):
			string1 += chr(c1)
		total = string1+" "*(20-len(string1))+string2
		w.encode_message(effect_byte=cmd_byte, message=total)
		w.add_silence(5000)

def test3():
	"""Test that told me that apparenly first byte of the message does diddly squat. or maybe it's a effect speed, but i don't care now, really"""
	for fst_byte in range(256):
		string1 = ""
		string2 = "1st byte test {0}".format(hex(fst_byte))
		for c1 in range(8):
			string1 += chr(c1)
		string1+="<= CGRAM"
		total = string1+" "*(20-len(string1))+string2
		w.encode_message(header_byte=fst_byte, message=total)
		w.add_silence(1000)
		print(string1, string2)


def test4():
	"""Overflow test. Diddly squat"""
	string1=""
	for c1 in range(64+20):
		string1 += chr(c1)
#	total = string1+" "*(20-len(string1))+string2
	w.encode_message(header_byte=0x20, effect_byte=0xe1, message=string1)
	w.add_silence(2000)
	w.encode_message(header_byte=0x20, effect_byte=0xe0, message="asdf\x01\x02\x03\x04\x05\x06\x07ASDFfdsa")
	w.add_silence(1000)

def bytestostr(byte_array):
	s = ""
	for x in range(len(byte_array)):
		s += str(byte_array[x])
	return s

def test5():
	"""Overflow test. Diddly squat"""
#	string1=""
#	for c1 in range(64+20):
#		string1 += chr(c1)
#	total = string1+" "*(20-len(string1))+string2
	for x in range(30):
		w.encode_message(header_byte=os.urandom(1)[0], effect_byte=os.urandom(1)[0], message= bytestostr(os.urandom(80)))
		w.add_silence(2000)
		w.encode_message(header_byte=os.urandom(1)[0], effect_byte=os.urandom(1)[0], message="asdf\x00\x01\x02\x03\x04\x05\x06\x07ASDFfdsa")
		w.add_silence(2000)


w.add_silence(2000)
test3()

w.write("./wtf5.wav")
		
